/*
 * Copyright © 2018 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

pub mod ir;

use ir::builder::Builder;
use ir::inst::*;

fn main() {
    let mut b = Builder::new();
    b.alu1(Opcode::MOV, Dst::lreg(Type::D, 0, 0, 1), Src::imm_ud(1));
    b.alu2(Opcode::ADD, Dst::lreg(Type::D, 0, 0, 1),
                        Src::lreg(Type::D, 0, 0), Src::imm_ud(1));
    b.alu2(Opcode::MUL, Dst::lreg(Type::D, 1, 0, 1),
                        Src::lreg(Type::D, 0, 0), Src::imm_ud(7));
    b.jump(Opcode::IF);
    b.alu2(Opcode::ADD, Dst::lreg(Type::D, 1, 0, 1),
                        Src::lreg(Type::D, 1, 0), Src::imm_ud(1));
    b.jump(Opcode::ELSE);
    b.alu2(Opcode::ADD, Dst::lreg(Type::D, 1, 0, 1),
                        Src::lreg(Type::D, 1, 0), Src::imm_ud(5));
    b.jump(Opcode::ENDIF);

    let s = b.make_shader();
    print!("{}", s);
}
