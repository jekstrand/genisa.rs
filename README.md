genisa.rs
=========

This is a little experimental project to play with new compiler ideas.  I've
been thinking of a variety of improvements we can make to the Intel back-end
compiler in mesa for some time now and this is a fun sand-box in which to play
with them.  One of those ideas is to see what would happen if you wrote
a compiler in [Rust](https://www.rust-lang.org).

Don't take anything in here too seriously!
